#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

struct people {
    string name = "unknown";
    string surname = "unknow";
    int price = 0;
    string date = "";
};

void tokenize(string str, char delim, vector<string> &out)
{
    size_t start;
    size_t end = 0;
    while ((start = str.find_first_not_of(delim, end)) != string::npos)
    {
        end = str.find(delim, start);
        out.push_back(str.substr(start, end - start));
    }
}

bool isNumber(string& s)
{
    for (char &ch : s) 
        if (isdigit(ch) == 0) return false;
    return true;
}

bool validate(people val){
    vector<string> date;
    tokenize(val.date, '.', date);
    if(date.size() != 3) return false;
    bool correct = true;
    for(int i = 0; i < date.size(); i++){
        if(!isNumber(date[i])) correct = false;
    }
    if(!correct) return false;
    return true;
}

int main(){
    string operation;
    string usr_string;
    people val;
    cout<<"Choose operation"<<endl;
    getline(cin,operation);
    if(operation == "list"){
        ifstream myfile;
        string line,
               nameMax,
               surnameMax, 
               dateMax;
        int sum = 0, 
            maxPrice = 0;

        myfile.open("list.txt", ios::binary);
        while (myfile >> val.name >> val.surname >> val.price >> val.date)
        {
            cout<<val.name<<" "<<val.surname<<" "<<val.price<<" "<<val.date<<endl;
            sum += val.price; 
            if(val.price > maxPrice) {
                maxPrice = val.price;
                nameMax = val.name;
                surnameMax = val.surname;
                dateMax = val.date;
            }
        }
        cout<<"Total amount : " << sum <<endl;
        cout<<"Maximum payout amount : " << maxPrice << " - " << nameMax << " " << surnameMax << " " << dateMax;
        myfile.close();
    } else if(operation == "add") {
        vector<string> out;
        cout << "Type new user: "<<endl;
        getline(cin,usr_string);

        if(usr_string != ""){
        tokenize(usr_string, ' ', out);
        val.name = out[0];
        val.surname = out[1];
        val.price = stoi(out[2]);
        val.date = out[3];
        bool correctValide = validate(val);
        if(correctValide){
            fstream usr_text("list.txt", ios::binary|ios::app);
            usr_text << usr_string << endl;
            usr_text.close();
        }else{ cout<<"Incorrect reading" << endl; }
        }else{ cout<<"Incorrect reading" << endl; }
    } else { cout<<"Incorrect operation"<<endl; }
    return 0;
}