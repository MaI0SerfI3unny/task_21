#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

struct vectorMath{
    float x;
    float y;
};

vectorMath add(vectorMath &a, vectorMath &b){
    vectorMath result;
    result.x = a.x + b.x;
    result.y = a.y + b.y; 
    return result;
}

vectorMath subtract(vectorMath &a, vectorMath &b){
    vectorMath result;
    result.x = a.x + b.x;
    result.y = a.y + b.y;
    return result;
}

vectorMath scalar(vectorMath &a, float scalar){
    vectorMath result;
    result.x = a.x * scalar;
    result.y = a.y * scalar;
    return result;
}

float length(vectorMath &a){
    return (float)sqrt((a.x * a.x) + (a.y * a.y));
}

float normalize(vectorMath &a){
	float locLength = length(a);
	float inv_length = (1 / locLength);
	a.x *= inv_length;
	a.y *= inv_length;
	return inv_length;
}


int main(){
    string operation;
    vectorMath a, b, result;
    cout<<"Choose operation: "<<endl;
    cin>>operation;
    if(operation == "add"){
        cout << "Type vector a"<<endl;
        cin>>a.x>>a.y;
        cout<< "Type vector b" <<endl;
        cin>>b.x>>b.y;
        result = add(a,b);
        cout << result.x << " " << result.y << endl; 
    }else if(operation == "subtract"){ 
        cout << "Type vector a"<<endl;
        cin>>a.x>>a.y;
        cout<< "Type vector b" <<endl;
        cin>>b.x>>b.y;
        result = subtract(a,b);
        cout << result.x << " " << result.y << endl;
    }else if(operation == "scale"){
        float scalarNum;
        cout<<"Type vector"<<endl;
        cin>>a.x>>a.y;
        cout<<"Enter scalar"<<endl;
        cin>>scalarNum;
        result = scalar(a,scalarNum);
        cout << result.x << " " << result.y <<endl;
    }else if(operation == "length"){
        cout<<"Type vector"<<endl;
        cin>>a.x>>a.y;
        cout << length(a) << endl;
    }else if(operation == "normalize"){
        cout<<"Type vector"<<endl;
        cin>>a.x>>a.y;
        cout << normalize(a) << endl;
    }else { 
        cerr<<"Incorrect operation"<<endl;
    }
    
    return 0;
}