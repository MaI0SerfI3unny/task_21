#include <iostream>
#include <vector>

using namespace std;

enum home_type 
{ 
    home, 
    storage,
    banya,
    garage
};

enum room_type 
{ 
    kitchen, 
    bed,
    bath,
    children,
    living
};

struct homeEquipment {
    bool truba = false;
    bool heat = false;
};

struct infoRoom{
    int level;
    int roomNum;
    int plochad;
    room_type type;
};

struct buildInfo{
    int level;
    int countRoom;
    int height;
    vector<infoRoom> roomArr;
};

struct build{
    int id;
    home_type type;
    int levelHome;
    vector <buildInfo> levelInfo;
    homeEquipment equip;
    int plochad = 0;
};

struct areaPlace{
    int countHome;
    int plochad;
    vector <build> arrBuild;
};

room_type setTypeRoom(int type){
    switch (type)
	{
	case 1:
		return room_type::kitchen;
		break;
	case 2:
		return room_type::bed;
		break;
	case 3:
		return room_type::bath;
		break;
	case 4:
		return room_type::children;
		break;
    case 5:
		return room_type::living;
		break;
	default:
	    return room_type::bed;
		break;
	}
}

home_type setTypeBuilding(int type){
    switch (type)
	{
	case 1:
		return home_type::home;
		break;
	case 2:
		return home_type::storage;
		break;
	case 3:
		return home_type::banya;
		break;
	case 4:
		return home_type::garage;
		break;
	default:
	    return home_type::home;
		break;
	}
}

int main(){
    int 
        countArea, 
        allArea=0, 
        allAreaBuild=0;
    cout << "Введите количество участков" << endl;
    cin>>countArea;
    vector <areaPlace> areaArr(countArea);
    for(int i = 0; i < countArea; i++){
        int countBuilding, plochad;
        cout<<"Участок №"<<i+1<<endl;
        cout<<"Введите количество постройек на участке"<<endl;
        cin>>countBuilding;
        cout<<"Введите площадь участка (кв.м^2)"<<endl;
        cin>>plochad;
        vector <build> typebuild(countBuilding);

        for(int buildItem = 0; buildItem < countBuilding; buildItem++){
            int 
             type,
             buildItemPlochad;

            cout<<"Введите тип дома №"<<buildItem+1<<endl;
            cin>>type;
            cout<<"Введите площадь дома"<<endl;
            cin>>buildItemPlochad;
            allAreaBuild += buildItemPlochad;
            build typeBuildItem;
            typeBuildItem.id = buildItem+1;
            typeBuildItem.type = setTypeBuilding(type);
            typeBuildItem.plochad = buildItemPlochad;
            
            if(typeBuildItem.type == home_type::home){
                int level;
                cout<<"Сколько этажей в доме"<<endl;
                cin>>level;
                typeBuildItem.levelHome = level;
                vector <buildInfo> objLevelInfo(level);
                for(int levelItem = 0; levelItem < level; levelItem++){
                    int room,height;
                    buildInfo objLevelItem;

                    cout<<"Укажите висоту потолка на " << levelItem+1 << " этаже" <<endl;
                    cin>>height;
                    
                    cout<<"Сколько комнат на "<<levelItem+1<<" этаже?"<<endl;
                    cin>>room;
                    
                    vector<infoRoom> roomsArr(room);
                    for(int roomItem = 0; roomItem < room; roomItem++){
                        infoRoom objRoomItem;
                        int typeRoomNum, ploschaRoom;
                        cout<<"Укажите тип комнаты "<<roomItem+1<<endl;
                        cin>>typeRoomNum;
                        cout<<"Укажите площадь комнаты"<<endl;
                        cin>>ploschaRoom;
                        objRoomItem.level = levelItem;
                        objRoomItem.roomNum = roomItem;
                        objRoomItem.plochad = ploschaRoom;
                        objRoomItem.type = setTypeRoom(typeRoomNum);
                        roomsArr[roomItem]= objRoomItem;
                    }

                    objLevelItem.countRoom = room;
                    objLevelItem.level = levelItem;
                    objLevelItem.roomArr = roomsArr;
                    objLevelItem.height = height;
                    objLevelInfo[i] = objLevelItem;

                }
                typeBuildItem.levelInfo = objLevelInfo;
            }

            if(typeBuildItem.type == home_type::home ||
               typeBuildItem.type == home_type::banya ){
                homeEquipment equipBuild;
                bool boolHeat, boolTruba;
                cout<<"Есть ли наличие труб в здании? (Yes: 1 / No: 0)"<<endl;
                cin>>boolTruba;
                cout<<"Есть ли наличие печи в здании? (Yes: 1 / No: 0)"<<endl;
                cin>>boolHeat;
                equipBuild.truba = boolTruba;
                equipBuild.heat = boolHeat;
                typeBuildItem.equip = equipBuild;
            }
            typebuild[buildItem] = typeBuildItem;
        }

        areaPlace areaItem;
        areaItem.countHome = countBuilding; 
        areaItem.plochad = plochad;
        areaItem.arrBuild = typebuild;
        areaArr[i] = areaItem;
        allArea += plochad;
    }

    cout<<"Общая площадь участка посёлка "<< allArea << " кв.м^2" << endl;
    cout<<"Общая площадь постройек - "<< (allAreaBuild*100) / allArea << "%" << endl;

    return 0;
}